package ir.droidhub.lifelog.main

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.bottomappbar.BottomAppBar
import ir.droidhub.lifelog.R
import kotlinx.android.synthetic.main.activity_bottom_sheet_content.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_main)

        fab.setOnClickListener { UserBottomSheet().show(supportFragmentManager, null) }
        findViewById<BottomAppBar>(R.id.bar).setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener, Toolbar.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {

                return true
            }
        })
    }
}